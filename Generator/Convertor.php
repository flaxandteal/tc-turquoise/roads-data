<?php

namespace ApiChecker;

require_once 'vendor/autoload.php';
use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Http;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use GuzzleHttp\Client;
use Ramsey\Uuid\Uuid;

class Converter {
    public $sector = "output";
    public $names;
    public $settings;
    public $FILEDIR = '/home/aidan/Documents/QGIS/Road Map/Sections&BuildingDetails/php-execrises/raw-sectors/9.geojson';
    public $finalDir = '/home/aidan/Documents/QGIS/Road Map/Sections&BuildingDetails/php-execrises/final.geojson';

    function __construct() {
        $this->names = new ArrayCollection();
        $this->settings = new ArrayCollection();
        $this->userInput();
    }

    function userInput() {

        $files = scandir('./raw-sectors/');

        foreach($files as $file) {

            if ($file == '.' || $file == '..') {
                continue;
            }

            $newNames = $this->getContents($file);

            echo "\n";
            echo strval($file) . "\n";
            echo "=========================\n";
            $minTime = readline("Enter min time: \n");
            $maxTime = readline("Enter max time: \n");
            $minThreshold = readline("Enter min threshold: \n");
            $maxThreshold = readline("Enter max threshold: \n"); //NEVER ABOVE 8

            $value = [$minTime, $maxTime, $minThreshold, $maxThreshold];

            $temp = new ArrayCollection(
                array_merge($newNames->toArray(), $this->names->toArray())
            );

            $this->settings->set($file, $value);

            $this->names = $temp;

            
        }

        print_r($this->names);
        $this->exportAllDataFormat();
    }

    function exportAllDataFormat() {
        $myfile = fopen("result/". $this->sector .".geojson", "w") or die("Unable to open file!");
        fwrite($myfile, "{\n");
        fwrite($myfile, "    \"data\": [\n");

        foreach ($this->names as $name => $file) {
            fwrite($myfile, "        { \"name\": \"" . $name . "\", \"details\": \n");
            fwrite($myfile, "            [\n");

            $values = $this->settings->get($file);

            $time = rand($values[0], $values[1]);
            $threshold = rand($values[2], $values[3]);

            fwrite($myfile, "                 {\"time\": 0, \"threshold\": 0.0},\n");
            fwrite($myfile, "                 {\"time\": ". $time .", \"threshold\": 0.". strval($threshold) ."},\n");

            $newThreshold = strval($threshold += rand(0, 2));

            if ($newThreshold == 10) {
                fwrite($myfile, "                 {\"time\": ". strval($time += rand(10, 500)) .", \"threshold\": 1}\n");
            } else {
                fwrite($myfile, "                 {\"time\": ". strval($time += rand(10, 500)) .", \"threshold\": 0.". strval($newThreshold) ."}\n");
            }

            fwrite($myfile, "            ]\n");
            fwrite($myfile, "        },\n");
        }

        fwrite($myfile, "    ]\n");
        fwrite($myfile, "}\n");
        
    }

    function getContents($file) {
        $data = file_get_contents('./raw-sectors/' . $file);
        
        $names = new ArrayCollection();
        $keepCount = new ArrayCollection();

        $rawNames = json_decode($data);

        for ($index = 0; $index < sizeof($rawNames->features); $index++) {
            $currentName = $rawNames->features[$index]->properties->Name;

            if ($keepCount->containsKey($currentName)) {
                $value = $keepCount->get($currentName);
                $keepCount->set($currentName, $value + 1);
                $value = $keepCount->get($currentName);

                $names->set($currentName . '_' . strval($value), $file);

            } else if ($currentName == '') {
                $myuuid = Uuid::uuid4();
                $names->set($myuuid->toString(), $file);
            } else {
                $names->set($currentName, $file);
                $keepCount->set($currentName, 1);
            }
        }
        
        return $names;
    }
}

$converter = new Converter;



?>