# INTRODUCTION
*Information*
Hello, welcome to the generator. This mini project is designed to take sections of buildings and convert these buildings into one single file (result/output) using the schema which I have been provided. This project will also check if the buildings have duplicated names and if they do, the project will add _02 at the end of the name for example A011, A011, A011 = A011, A011_02, A011_03. This project will also check if the buildings have names and will add a UUID if the building name does not exists.

*Schema*
{
    "data": [
        { "name": "Building1", "details": 
            [
                {"time": 0, "threshold": 0.0},
                {"time": 4500, "threshold": 0.0},
                {"time": 4530, "threshold": 0.7}
            ]
    ]
}

# START
*Installation*
Please make sure you have the following installed and functionally properly:
- Code runner (Option if you want to use the terminal to run instead)
- Composer
- QGIS
- Road map project from gitlab

*Getting Sections*
Next will have to created our sectors, please load the road map project within QGIS, find the select tool (https://i.stack.imgur.com/rHAdp.png). Next start highlighting the buildings you wish to select. Please keep in mind that each section will have different settings for example time range and threshold range. Once you have selected your buildings, right click on the buildings within the layers panel, "Export -> Save Selected Feature As", give your file a name and save. Next place file within the raw-sectors folder within this project.

*Generating*
Run the convertor.php class and it should allow you to input the minimal time, maximum time, minimal threshold and maximum threshold for each section. Once you have inputted all these values, the file will automatically generate within result/output.geojson.