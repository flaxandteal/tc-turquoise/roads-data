# INTRODUCTION
This is all the GEOJSON coordinates road markers. This will help developers to allow the user to move throughout 
the city, generate road textures, health of roads, etc.

All the lines you can see represent the roads. These roads are broken down into three different colours to represent 
the three different road types:

- Green = Road is concrete
- Pink = Road is gravel
- Blue = Road is dirt

There is also a quality number for each road ranging from 5 being the best and 1 being the worst. If you wish to see the
quality of a road or all roads, please follow the steps:

1. Right click on "roads" within the layers tab
2. Select Open Attribute Table

You can see all the roads type and quality. You can also click on a number within the indexing numbers on the left
hand side to show which road that attribute belongs to for example clicking on number 16 highlighted a road on map
so number 16 attributes belong to that highlighted road. 

*Continue to see single road quality*

3. Find the select feature tool within the main window interface and select a road within the map
https://i.stack.imgur.com/rHAdp.png

4. Once selected and the road is highlighted, check your attribute table and the row should be highlighted which relates
to that road

# INSTALLATION
1. Please make sure you have QGIS install on your machine
2. Once installed and opened Project->Open->Project.qgz

# GOAL
The goal of this project is to place roads in suitable areas where the user can atleast see most of the houses 
being destroyed within the natural disaster.

@ Author: Aidan McErlean
@ Galvia Digital

# VERSION 
1.3.0
